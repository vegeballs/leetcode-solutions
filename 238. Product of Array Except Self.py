class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        start = 1
        end = 1 
        ret = [1] * len(nums)
        for i in range(len(nums)):
            ret[i] *= start
            start *= nums[i]
            ret[~i] *= end
            end *= nums[~i]
        return ret