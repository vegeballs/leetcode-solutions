class Solution:
    def findDisappearedNumbers(self, nums: List[int]) -> List[int]:
        if len(nums) > 0:
            maxNum = len(nums)
            ret = list(set(range(1,maxNum + 1)) - set(nums))
            return ret
        else:
            return []